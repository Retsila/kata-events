import { makeStyles } from '@material-ui/core'

export const useStyles = makeStyles((theme) => ({
  daysContainer: {
    borderStyle: 'solid',
    borderWidth: '1px',
    height: '100%',
    borderRightStyle: 'none',
  },
  dayColumn: {
    borderWidth: '1px',
    borderRightStyle: 'solid',
    position: 'relative',
    display: 'flex',
  },
}))
