import { makeStyles } from '@material-ui/core'

export const useStyles = makeStyles((theme) => ({
  scheduleColumn: {
    alignItems: 'center',
    flexDirection: 'column',
    borderTopStyle: 'solid',
    borderLeftStyle: 'solid',
    borderWidth: '1px',
  },
  hourBox: {
    borderWidth: '1px',
    borderBottomStyle: 'solid',
    width: '100%',
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
  },
}))
