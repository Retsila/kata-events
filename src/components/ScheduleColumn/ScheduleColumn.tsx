import { Grid } from '@material-ui/core'
import { activeHours } from '../../utils'
import { Line } from '../Line'
import React from 'react'
import { useStyles } from './ScheduleColumn.style'

export const ScheduleColumn = (): JSX.Element => {
  const classes = useStyles()
  return (
    <Grid item xs={1} container className={classes.scheduleColumn}>
      {activeHours.map((id) => (
        <Grid item key={id} className={classes.hourBox}>
          <span style={{ height: '50%' }}>{`${id + 9} heures`}</span>
          <Line />
        </Grid>
      ))}
    </Grid>
  )
}
