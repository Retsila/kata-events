import React from 'react'

export const Line = (): JSX.Element => (
  <hr
    style={{
      margin: 0,
      color: 'black',
      backgroundColor: 'black',
      height: 1,
      width: '100%',
      borderWidth: '1px',
      border: 0,
    }}
  />
)
export default Line
