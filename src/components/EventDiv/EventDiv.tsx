import React from 'react'
import { EventDivData } from '../../App'

interface EventDivProps {
  eventDivData: EventDivData
  columnWidth: number
  pxPerMinute: number
}

export const EventDiv = (props: EventDivProps): JSX.Element => {
  const { eventDivData, columnWidth, pxPerMinute } = props
  const top = eventDivData.start * pxPerMinute
  const left =
    eventDivData.position *
    ((parseFloat(eventDivData.width) / 100) * columnWidth)
  const borderRightStyle =
    eventDivData.position === eventDivData.overlappedId.length
      ? 'none'
      : 'solid'
  return (
    <div
      style={{
        borderWidth: '1px',
        borderStyle: 'solid',
        position: 'absolute',
        zIndex: -1,
        backgroundColor: 'yellow',
        height: eventDivData.duration * pxPerMinute,
        top: top,
        left: left,
        width: eventDivData.width,
        borderTopStyle: top ? 'solid' : 'none',
        borderLeftStyle: left ? 'solid' : 'none',
        borderRightStyle: left ? borderRightStyle : 'solid',
      }}
    >
      {eventDivData.id}
    </div>
  )
}
