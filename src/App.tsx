import React, { useEffect, useMemo, useRef, useState } from 'react'
import { Grid } from '@material-ui/core'
import { activeDays, areOverlapping, convertStartTimeToMinutes } from './utils'
import { calendarDuration, inputDataTest as inputData } from './constants'
import { EventDiv } from './components/EventDiv'
import { useStyles } from './App.style'
import { ScheduleColumn } from './components/ScheduleColumn'

export interface EventDivData {
  id: number
  start: number
  end: number
  duration: number
  position: number
  width: string
  overlappedId: EventDivData[]
}

function App() {
  const classes = useStyles()
  const columnsRef = useRef<HTMLDivElement[]>([])
  const [columnHeight, setColumnHeight] = useState<number>()
  const [columnWidth, setColumnWidth] = useState<number>()

  const getMeasurements = () => {
    if (columnsRef?.current[0]) {
      const newColumnsHeight: number = columnsRef.current[0].clientHeight
      setColumnHeight(newColumnsHeight)
      const newColumnWidth: number = columnsRef.current[0].clientWidth
      setColumnWidth(newColumnWidth)
    }
  }
  useEffect(() => {
    getMeasurements()
  }, [])
  useEffect(() => {
    window.addEventListener('resize', getMeasurements)
    return () => {
      window.removeEventListener('resize', getMeasurements)
    }
  }, [])

  const getPxPerMinute = useMemo(
    (): number => (columnHeight ? columnHeight / calendarDuration : 0),
    [columnHeight]
  )

  const buildEventsData: EventDivData[] = useMemo(() => {
    const eventsData: EventDivData[] = inputData
      .map((v): EventDivData => {
        const start = convertStartTimeToMinutes(v.start)
        return {
          id: v.id,
          start: start,
          end: start + v.duration,
          duration: v.duration,
          position: -1,
          width: '100%',
          overlappedId: [],
        }
      })
      .sort((a, b) => {
        const result = a.start - b.start
        return result ? result : a.end - b.end
      })
    for (let i = 0; i < eventsData.length; i++) {
      const currentEvent = eventsData[i]
      let j = i + 1

      while (
        j < eventsData.length &&
        areOverlapping(currentEvent, eventsData[j])
      ) {
        eventsData[j].overlappedId.push(currentEvent)
        j += 1
      }
      let k = 0
      while (
        k <= currentEvent.overlappedId.length &&
        currentEvent.position === -1
      ) {
        let isPresent = false
        for (const e of currentEvent.overlappedId) {
          isPresent = isPresent || e.position === k
        }
        if (!isPresent) {
          currentEvent.position = k
        }
        k += 1
      }
      const width = 100 / (currentEvent.overlappedId.length + 1)
      currentEvent.width = width.toString().concat('%')
      for (const overlappedEvents of currentEvent.overlappedId) {
        overlappedEvents.width = currentEvent.width
      }
    }
    return eventsData
  }, [])

  return (
    <Grid id="main" container style={{ padding: '5%', height: '100vh' }}>
      <ScheduleColumn />
      <Grid container item xs={11} className={classes.daysContainer}>
        {activeDays.map((id) => (
          <Grid
            id={`column${id}`}
            ref={(el) => (columnsRef.current[id] = el as HTMLDivElement)}
            key={id}
            item
            xs={2}
            className={classes.dayColumn}
          >
            {id === 0 &&
              buildEventsData.map(
                (v) =>
                  columnWidth && (
                    <EventDiv
                      key={v.id}
                      eventDivData={v}
                      columnWidth={columnWidth}
                      pxPerMinute={getPxPerMinute}
                    />
                  )
              )}
          </Grid>
        ))}
      </Grid>
    </Grid>
  )
}

export default App
