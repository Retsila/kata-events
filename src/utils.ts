import { calendarBeginningHour } from './constants'
import { EventDivData } from './App'

export const activeDays = Array.from({ length: 6 }, (v, i) => i)
export const activeHours = Array.from({ length: 12 }, (v, i) => i)

export const convertStartTimeToMinutes = (startTime: string): number => {
  const [hour, minutes] = startTime
    .split(':', 2)
    .map((value) => parseInt(value))
  return (hour - calendarBeginningHour) * 60 + minutes
}

export const areOverlapping = (A: EventDivData, B: EventDivData) => {
  if (B.start < A.start) {
    return B.end > A.start
  } else {
    return B.start < A.end
  }
}
